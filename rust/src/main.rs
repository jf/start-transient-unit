// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>
// SPDX-License-Identifier: MIT
use std::error::Error;
use zvariant::{OwnedObjectPath, Value};
use std::rc::Rc;
use futures::stream::{self, StreamExt, TryStreamExt};
use std::io;
use std::cell::RefCell;

use gio::prelude::*;

struct State {
    launch_ctx: gio::AppLaunchContext,
    pending: RefCell<Vec<gio::AppInfo>>
}

fn main() -> Result<(), Box<dyn Error>> {
    let ctx = glib::MainContext::new();
    ctx.push_thread_default();
    let lp = glib::MainLoop::new(Some(&ctx), false);
    ctx.spawn_local(glib::clone!(@strong lp => async move {
        let state = Rc::new(State {
            launch_ctx: gio::AppLaunchContext::new(),
            pending: RefCell::new(Vec::new())
        });
        setup(state.clone(), lp).await.unwrap();
        let files = std::env::args().skip(1).map(gio::File::for_path).collect::<Vec<gio::File>>();
        launch(state.clone(), &files).await.unwrap();
    }));
    lp.run();
    ctx.pop_thread_default();
    Ok(())
}

fn escape_systemd_string(s: &str) -> String {
    let mut escaped = String::new();
    for c in s.bytes() {
        if c == b'/' {
            escaped.push('-');
        } else if c.is_ascii_alphanumeric() || c == b':' || c == b'_' || c == b'.' {
            escaped.push(c as char);
        } else {
            escaped.push_str(format!("\\x{:02x}", c).as_str())
        }
    }
    escaped
}

async fn setup(state: Rc<State>, lp: glib::MainLoop) -> Result<(), Box<dyn Error>> {
    let conn = zbus::azync::Connection::new_session().await?;
    let manager = Rc::new(AsyncSystemdManagerProxy::new(&conn)?);
    let launch_ctx = state.launch_ctx.clone();
    let pending = state.pending.clone();
    launch_ctx.connect_launched(move |_, info, platform_data| {
        let ctx = glib::MainContext::ref_thread_default();
        let m = manager.clone();
        pending.borrow_mut().push(info.clone());
        ctx.spawn_local(glib::clone!(@strong m, @strong info, @strong platform_data, @strong pending, @strong lp => async move {
            let dict = platform_data.get::<glib::VariantDict>().unwrap();
            if let Some(vpid) = dict.lookup_value("pid", Some(unsafe { glib::VariantTy::from_str_unchecked("i") })) {
                let pid = vpid.get::<i32>().unwrap();
                start_systemd_scope(m, info.clone(), pid).await;
                glib::g_print!("pid for \"{}\" spawned: {}\n", info.name(), pid);
            } else {
                glib::g_print!("appinfo \"{}\" spawned\n", info.name());
            }
            pending.borrow_mut().retain(|i| *i != info);
            if pending.borrow().is_empty() {
                lp.quit();
            }
        }));
    });
    Ok(())
}

async fn launch(state: Rc<State>, files: &[gio::File]) -> Result<(), Box<dyn Error>> {
    let launch_ctx = &state.launch_ctx;
    stream::iter(files).then(|f| async move {
        let finfo = f.query_info_async_future(&gio::FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                                             gio::FileQueryInfoFlags::NONE,
                                             glib::PRIORITY_DEFAULT).await?;
        let path = f.peek_path().unwrap();
        let mime = finfo.content_type()
            .ok_or_else(|| io::Error::new(io::ErrorKind::NotFound, path.clone().to_string_lossy()))?;
        let ainfo = gio::AppInfo::default_for_type(mime.as_str(), false)
            .ok_or_else(|| io::Error::new(io::ErrorKind::NotFound, format!("{}: {}", &path.to_string_lossy(), mime.as_str())))?;
        ainfo.launch(&[f.clone()], Some(launch_ctx))?;
        Ok(())
    }).inspect_err(|err| glib::g_printerr!("failed to launch: {}\n", err))
    .collect::<Vec<Result<(), Box<dyn Error>>>>().await;
    Ok(())
}

async fn start_systemd_scope(manager: Rc<AsyncSystemdManagerProxy<'_>>, info: gio::AppInfo, pid: i32) {
    let mut props = Vec::new();
    if let Some(desc) = info.description() {
        props.push(("Description", Value::Str(String::from(desc).into())));
    }
    props.push(("PIDs", Value::Array(vec![pid as u32].into())));
    props.push(("CollectMode", Value::Str("inactive-or-failed".into())));
    let escaped_name = escape_systemd_string(&info.name());
    let unit = format!("rust-systemd-run-{}-{}.scope", escaped_name, pid);
    let res = manager.start_transient_unit(&unit, "fail", props, Vec::new()).await;
    match res {
        Ok(path) => glib::g_print!("systemd unit spawned: {}\n", path.as_str()),
        Err(err) => glib::g_printerr!("failed to spawn systemd unit: {}\n", err)
    }
}

#[zbus::dbus_proxy(
    interface = "org.freedesktop.systemd1.Manager",
    default_service = "org.freedesktop.systemd1",
    default_path = "/org/freedesktop/systemd1"
)]
trait SystemdManager {
    fn start_transient_unit(
        &self,
        name: &str,
        mode: &str,
        properties: Vec<(&str, Value<'_>)>,
        aux: Vec<(&str, Vec<(&str, Value<'_>)>)>) -> zbus::Result<OwnedObjectPath>;
}

