// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>
// SPDX-License-Identifier: MIT
// modules: Gee-0.8

public static void main(string[] argv) {
  var ctx = new MainContext();
  ctx.push_thread_default();
  var loop = new MainLoop(ctx, false);
  var files = new Gee.LinkedList<File>();
  files.add_all_iterator(new Gee.ArrayList<string>.wrap(argv).chop(1)
    .map<File>(s => File.new_for_path(s)));
  async_main.begin(loop, files);
  loop.run();
  ctx.pop_thread_default();
}

async void async_main(MainLoop loop, Gee.Collection<File> files) throws IOError {
  if (files.is_empty) {
    loop.quit();
    return;
  }
  var launch_ctx = new AppLaunchContext();
  var bus = yield Bus.get(SESSION);
  var pending = new Gee.LinkedList<AppInfo>();
  launch_ctx.launched.connect((info, platform_data) => {
    Variant? pid = platform_data.lookup_value("pid", VariantType.INT32);
    if (pid == null)
      return;
    var p = pid.get_int32();
    print("got pid %d for app \"%s\"\n", p, info.get_name());
    start_systemd_scope.begin(info, p, loop, bus, pending);
  });
  var counter = files.size;
  foreach (var file in files) {
    launch.begin(file, loop, launch_ctx, pending, (obj, res) => {
      launch.end(res);
      counter--;
      if (counter == 0 && pending.is_empty)
        loop.quit();
    });
  }
}

async void start_systemd_scope(AppInfo info, int32 pid, MainLoop loop, DBusConnection bus, Gee.Collection<AppInfo> pending) {
  pending.add(info);
  try {
    yield Gnome.start_systemd_scope(info.get_name(), pid, info.get_description(), bus, null);
  } catch (Error error) {
    printerr("launching scope \"%s\" failed: %s\n", info.get_name(), error.message);
  }
  pending.remove(info);
  if (pending.is_empty)
    loop.quit();
}

async void launch(File file, MainLoop loop, AppLaunchContext launch_ctx, Gee.Collection<AppInfo> pending) {
  try {
    var finfo = yield file.query_info_async(FileAttribute.STANDARD_CONTENT_TYPE, FileQueryInfoFlags.NONE, Priority.DEFAULT);
    var mime = finfo.get_content_type();
    var ainfo = AppInfo.get_default_for_type(mime, true);
    var uris = new List<string>();
    uris.append(file.get_uri());
    pending.add(ainfo);
    try {
      yield ainfo.launch_uris_async(uris, launch_ctx, null);
    } finally {
      pending.remove(ainfo);
      if (pending.is_empty)
        loop.quit();
    }
  } catch (Error e) {
    printerr("opening \"%s\" failed: %s\n", file.get_uri(), e.message);
  }
}

